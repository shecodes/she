#!/usr/bin/pypy2

# coding: utf-8

# Copyright 2020, 2021, 2022, mn, fcr

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

import os
from sys import argv, stdin
import Tkinter as tk
import tkFileDialog as filedialog

class Menubar:

    def __init__(self,parent):
        menubar = tk.Menu(parent.master)
        parent.master.config(menu = menubar)
        file_dropdown = tk.Menu(menubar)
        file_dropdown.add_command(label = "New File", command = parent.new_file, accelerator = "Ctrl+N")
        file_dropdown.add_command(label = "Open File", command = parent.open_file, accelerator = "Ctrl+O")
        file_dropdown.add_command(label = "Save", command = parent.save, accelerator = "Ctrl+S")
        file_dropdown.add_command(label = "Save As", command = parent.save_as, accelerator = "Ctrl+Shift+S")
        file_dropdown.add_separator()
        file_dropdown.add_command(label = "Quit", command = parent.master.destroy, accelerator = "Ctrl+Q")

        about_dropdown = tk.Menu(menubar)
        about_dropdown.add_command(label = "About", command = parent.about)
        menubar.add_cascade(menu = file_dropdown, label = "File")
        menubar.add_cascade(label = "About/Jankari", menu = about_dropdown )
   
class PyText:
    
    def __init__(self,master):
        self.ver = "1.5"
        master.title("untitled - she " + self.ver + " - path:" + os.getcwd())
        #master.geometry("700x500")

        self.master = master
        self.filename = None

        self.textarea = tk.Text(master)
        self.scroll = tk.Scrollbar(master, command = self.textarea.yview)
        self.textarea.configure(yscrollcommand = self.scroll.set)
        self.textarea.configure(font=("monospace", 16), bg="#e7e7e7")
        self.textarea.pack(side = tk.LEFT,fill = tk.BOTH,expand = True)
        self.scroll.pack(side = tk.RIGHT , fill = tk.Y)
        self.menubar = Menubar(self)

        self.bind_shortcuts()

        pst = ""
        if not os.isatty(stdin.fileno()):
            for p in stdin: 
                pst += str(p[:-1]) + chr(10)
            self.textarea.insert(1.0, pst)

        try:
            if argv[1:][0]:
                self.filename = argv[1:][0]
                with open(self.filename, "r") as f:
                    self.textarea.insert(1.0, f.read())
                self.set_window_title(self.filename + " - she " + self.ver + " - path:" + os.getcwd())
        except:
            pass

    def set_window_title(self,name = None):
        if name:
            self.master.title(name)
        else:
            self.master.title("she " + self.ver + " - Untitled - path:" + os.getcwd())

    def new_file(self,*args):
        self.textarea.delete(1.0,tk.END)
        self.filename = None
        self.set_window_title()

    def about(self,*args):
        nl = chr(10)
        text = "# about she " + self.ver + "\n\n# she is a text editor based on ej, which lets you run shell commands in the editor. to run the text on the current line, just use ctrl+t.\n\n# copyright 2020, 2021, 2022, 2024 mn, fcr\n\n# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\n# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/."
        self.textarea.insert(tk.INSERT, nl + text + " " + nl)

    def qu(self,*args):
        master.destroy()

    def ctrlt(self,*args):
        nl = chr(10) ; from os import popen as po
        try: 
            selectflag = 0
            sel = [""]
            sel = self.textarea.get(tk.SEL_FIRST, tk.SEL_LAST).split(nl)
            selectflag = 1
            cmd = sel[0].rstrip()
        except: 
            cmd = self.textarea.get(1.0,tk.END).split(nl)
            cmd = cmd[int(self.textarea.index(tk.INSERT).split(".")[0]) - 1]
        try: 
            first = cmd.split()[0].lower()
            amp = (chr(32) + cmd.split(nl)[0]).rstrip()[-1]
        except: 
            first = ""
            amp = ""
        if first == "cd":
            try: second = cmd.split()[1] ; os.chdir(second) ; self.textarea.insert(tk.INSERT, nl + " " + nl)
            except: pass
            self.set_window_title("she " + self.ver + " - path:" + os.getcwd())
        if cmd.strip() == "cls":
            self.textarea.delete(tk.INSERT,tk.END)
            self.textarea.insert(tk.END, chr(32) + chr(10))
        elif first == "appto":
            try:
                second = cmd.split()[1]
                added = ""
                with open(second, "a") as outfile:
                    for each in sel[1:]:
                        outfile.write(each + nl)
                    outfile.close()
                    added = nl + "= added ="
                self.textarea.insert(tk.INSERT, added + nl + " " + nl)
            except: pass

        elif first == "mk":
            eno = -1
            import sys
            try:
                second = cmd.lstrip()[len("mk") + 1:].split("|")
            except: second = []
            try:
                eno = 0 
                f = []
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                bdir = os.getcwd()
                udir = '/home/' + os.getlogin() + '/she'
                try:
                    os.chdir('/root/she')
                    udir = '/root/she'
                except:
                    pass
                os.chdir(bdir)
                try:
                    if not udir in sys.path:
                        sys.path.insert(0, udir)
                    import custom
                    from custom import mkcustom
                    reload(custom)
                    from custom import mkcustom
                    f = mkcustom(selget, second)
                except: 
                    f = [udir + '/custom.py error']
                os.chdir(bdir)
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "sort":
            try:
                f = []
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                selget.sort()
                for each in selget:
                    f += [each]
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "rev":
            try:
                f = []
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                for each in selget:
                    f += [each[::-1]]
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "seek":
            try:
                second = cmd.lstrip()[len("seek") + 1:].split("|")
                f = []
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                for each in selget:
                    for parts in second:
                        if parts.lower() in each.lower():
                            f += [each]
                            break
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "unseek":
            try:
                second = cmd.lstrip()[len("unseek") + 1:].split("|")
                f = []
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                for each in selget:
                    flag = 1
                    for parts in second:
                        if parts.lower() in each.lower():
                            flag = 0
                            break
                    if flag:
                        f += [each]
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "replace":
            try:
                second = cmd.lstrip()[len("replace") + 1:].split("|")
                f = [] 
                rfrom = second[0].replace("\\n", "\n")
                rfto = second[1].replace("\\n", "\n")
                if selectflag:
                    selget = sel[1:]
                else:
                    selget = self.textarea.get(1.0,tk.END).split(nl)
                for each in "\n".join(selget).replace(rfrom, rfto).split("\n"):
                    f += [each]
            except: pass
            self.textarea.insert(tk.INSERT, nl + "\n".join(f) + " " + nl)

        elif first == "wcl":
            try:
                if selectflag:
                    f = sel
                else:
                    f = self.textarea.get(1.0,tk.END).split(nl)[1:]
            except: pass
            self.textarea.insert(tk.INSERT, nl + str(len(f) - 1) + " " + nl)
        else:
            if amp == "&":
                f = os.system(cmd.split(nl)[0].encode('utf-8'))
                self.textarea.insert(tk.INSERT, nl + " " + nl)
            else:
                f = po(cmd.split(nl)[0].encode('utf-8'))
                self.textarea.insert(tk.INSERT, nl + f.read().rstrip() + " " + nl)

    def open_file(self,*args):
        self.filename = filedialog.askopenfilename(
        defaultextension = "",
        filetypes = [("All Files","*.*"),
                   ("Text File","*.txt"),
                   ("Python file","*.py"),
                   ("FIG ","*.fig"),
                   ("FIG PLUS ","*.fgp"),
                   ("PAVUM","*.pav"),
                   ("JavaScript","*.js"),
                   ("HTML Documents","*.html"),
                   ("CSS  Document","*.css"),
                   ("Markdown Document","*.*")] )

        if self.filename:
            self.textarea.delete(1.0,tk.END)
            with open(self.filename, "r") as f:
                self.textarea.insert(1.0, f.read())
            self.set_window_title(self.filename + " - she " + self.ver + " - path:" + os.getcwd())

    def save(self,*args):
        if self.filename:
            try:
                textarea_content = self.textarea.get(1.0,tk.END)
                with open(self.filename,"w") as f:
                    f.write(textarea_content.encode('utf-8'))

            except Exception as e :
                print(e)
        else:
            self.save_as()

    def save_as(self,*args):
        try:
            new_file = filedialog.asksaveasfilename(
            initialfile = "untitled.txt",
            defaultextension = ".txt",
            filetypes = [("All Files", "*.*"),
                ("Text File", "*.txt"),
                ("Python file", "*.py"),
                ("FIG ", "*.fig"),
                ("FIG PLUS ", "*.fgp"),
                ("PAVUM", "*.pav"),
                ("JavaScript", "*.js"),
                ("HTML Documents", "*.html"),
                ("CSS  Document", "*.css"),
                ("Markdown Document", "*.*")] )

            textarea_content = self.textarea.get(1.0,tk.END)
            with open(new_file, "w") as f:
                f.write(textarea_content.encode('utf-8'))
            self.filename = new_file
            self.set_window_title(self.filename)

        except Exception as e:
            print(e)

    def bind_shortcuts(self):
        self.textarea.bind('<Control-n>', self.new_file)
        self.textarea.bind('<Control-o>', self.open_file)
        self.textarea.bind('<Control-s>', self.save)
        self.textarea.bind('<Control-S>', self.save_as)
        self.textarea.bind('<Control-b>', self.ctrlt)
        self.textarea.bind('<Control-t>', self.ctrlt)
        self.textarea.bind('<Control-q>', self.qu)

if __name__ == "__main__":
    master = tk.Tk()
    pt = PyText(master)
    master.mainloop()

